# Tribeca provisioner

### You will need 
- VirtualBox
- Vagrant

#### clone this repo
git clone git@bitbucket.org:evaldosantosjoyjet/tribeca-provision.git  
git submodule init  
git submodule update  

#### after this, run
vagrant up